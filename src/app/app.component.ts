import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthService } from './auth.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor( private http: Http, private auth: AuthService) {
    // let url = 'https://api.spotify.com/v1/search?type=album&query=metal';
    
    // url = 'https://api.spotify.com/v1/albums/0sNOF9WDwhWunNAHPD3Baj'
    // url = 'https://api.spotify.com/v1/me'
    // url = 'https://api.spotify.com/v1/tracks/5FVd6KXrgO9B3JPmC8OPst'
    // var w = 5;
    // console.log(w)

    // this.http.get(url, {
    //   headers: new Headers({
    //     'Authorization': 'Bearer ' + auth.getToken()
    //   })
    // }).subscribe( response => {
    //   console.log(response.json());
    // });
    // this.http.get('https://jsonplaceholder.typicode.com/posts', {}).subscribe(response => {
    //   console.log(response);
    // })
  }

}
