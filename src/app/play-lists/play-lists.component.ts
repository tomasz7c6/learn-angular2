import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators  } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser';


@Component({
  selector: 'app-play-lists',
  templateUrl: './play-lists.component.html',
  styleUrls: ['./play-lists.component.css']
})
export class PlayListsComponent implements OnInit {

  searchForm: FormGroup;

  constructor() {
    this.searchForm = new FormGroup({
      'query': new FormControl('Batman')
    });
    console.log('this.searchForm', this.searchForm)
  }

  // this.searchForm.query

  sendForm(param) {
    console.log(param)
  }

  // formControlName="query"
  someVal = "FFF"
  ngOnInit() {
  }

}
