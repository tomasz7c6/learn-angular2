import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayListsComponent } from './play-lists.component';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routerModule } from './play-list.routing'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    // FormGroup, 
    // FormControl, 
    ReactiveFormsModule,
    routerModule
  ],
  declarations: [PlayListsComponent],
  exports: [
    PlayListsComponent
  ]
})
export class PlayListsModule { }
