import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PlayListsModule } from './play-lists/play-lists.module'
import { AuthService } from './auth.service';
import { MusicSearchModule } from './music-search/music-search.module';

import { routerModule } from './app.routing';
import { NavBarComponent } from './common/nav-bar/nav-bar.component';
import { TeamModule } from './team/team.module';
// import { BreadcrumbComponent } from './common/breadcrumb/breadcrumb.component';

import { TeamService } from "./team/team.service";
import { MusicSharedModule } from "./music-shared/music-shared.module";

import { TeamSelectionService } from "./music-shared/team-selection.service";




@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    // BreadcrumbComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    PlayListsModule,
    MusicSearchModule,
    MusicSharedModule,
    TeamModule,
    routerModule
  ],
  providers: [
    AuthService,
    TeamService,
    TeamSelectionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private auth: AuthService) {
    console.log(auth)
    auth.getToken();
  }

}
