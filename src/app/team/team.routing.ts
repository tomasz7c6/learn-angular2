import { RouterModule, Routes } from '@angular/router';
// import { PlayListsComponent } from './../play-lists/play-lists.component';
import { TeamComponent } from './team.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamFormComponent } from "./team-form/team-form.component";

const routesConfig: Routes = [
    {
        path: 'team',
        component: TeamComponent,
        data: {
            breadcrumb: "Team"
        },
        children: [
            // {
            //     path: '',
            //     component: TeamDetailsComponent,
            //     data: {
            //       breadcrumb: "Details"
            //     }
            // },
            {
                path: 'new',
                component: TeamFormComponent,
                data: {
                  breadcrumb: "New"
                }
            },
            {
                path: ':id',
                component: TeamDetailsComponent,
                data: {
                  breadcrumb: "Details"
                }
            },
            {
                path: ':id/edit',
                component: TeamFormComponent,
                data: {
                  breadcrumb: "Edit"
                }
            }
        ]
    }
]

export const routerModule = RouterModule.forChild(routesConfig)