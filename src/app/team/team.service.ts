import { Injectable, Optional, Inject } from '@angular/core';
import { Http } from "@angular/http";
import { Subject, Observable } from "rxjs/Rx";


export interface TeamList{
  name: string,
  players: any[],
  favourites: boolean,
  sponsor: string
}


@Injectable()
export class TeamService {

  teamList = [];

  server_url = 'http://localhost:3000/playlists/';

  // constructor(@Optional() @Inject('TeamData') teamData) {
  //   this.teamList = teamData === null? this.teamList : teamData;
  // }

  constructor(private http:Http) {}

  addToTeamList(teamId, track) {
    console.log('teamId, track', teamId, track)
    let teamPlayers = this.teamList.find(teamlist => teamlist.id == teamId);
    teamPlayers.players.push(track);

    this.saveTeam(teamPlayers).subscribe(() => {
      // ...
    });
  }

  public createNewTeam():TeamList {
    return {
      name: '',
      players: [],
      favourites: false,
      sponsor: ''
    }
  }

  private getTeam() {
    return this.http.get(this.server_url)
    .map(response => response.json())
    .subscribe(response => {
      this.teamList = response;
      this.teamListStream$.next(this.teamList);
    });
  }
  
  private teamListStream$ = new Subject<TeamList[]>();

  public getTeamListStream() {
    if(!this.teamList.length) {
      this.getTeam();
    }
    return this.teamListStream$.startWith(this.teamList)
  }

  public getTeamItem(id) {
    return this.http.get(this.server_url + id)
    .map(response => response.json());
  }

  public deleteTeam(id) {
    console.log('id', id)
    this.http.delete(this.server_url + id)
    .map(resp => resp.json())
    .subscribe(resp => {
      this.getTeam();
    });
  }

  public saveTeam(playlist) {
    let request;
    if(playlist.id) {
      request = this.http.put(this.server_url + playlist.id, playlist);
    } else {
      request = this.http.post(this.server_url, playlist);
    }
    return request.map(resp => resp.json()).do(resp=>{
      this.getTeam();
    });
  }

}
