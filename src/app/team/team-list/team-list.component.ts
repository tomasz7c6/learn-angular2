import { Component, OnInit, Inject } from '@angular/core';
import { TeamService, TeamList } from './../team.service'

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css'],
  styles: [`
    table {
      margin-top: 50px;
    }
  `]
})
export class TeamListComponent implements OnInit {

  teamList = [];

  constructor(private teamService: TeamService) {
  }



  ngOnInit() {
    // this.teamList = this.teamService.getTeam();
    this.teamService.getTeamListStream()
      .subscribe((response: TeamList[]) => {
        this.teamList = response;
      });

    //lub 
    // this.teamList = this.teamService.getTeamListStream()
    //ale wtedy w ngFor należy dać pipe acync
  }

  deleteTeam(id) {
    this.teamService.deleteTeam(id);
  }

}
