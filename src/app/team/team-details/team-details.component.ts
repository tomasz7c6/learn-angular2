import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { TeamService, TeamList } from "./../team.service";

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styles: [`
    .details {
      margin-top: 50px;
    }
  `]
})
export class TeamDetailsComponent implements OnInit {

  teamDetail: TeamList;

  constructor(private activeRoute: ActivatedRoute, private teamService: TeamService) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      let id = parseInt(params['id']);
      if (id) {
        this.teamService.getTeamItem(id)
          .subscribe((resp: TeamList) => {
            this.teamDetail = resp;
          },
          error => {
            console.log(error);
          });
      }
    });
  }

}
