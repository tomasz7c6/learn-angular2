import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamComponent } from './team.component';
import { routerModule } from './team.routing';
import { TeamListComponent } from './team-list/team-list.component'
import { TeamDetailsComponent } from './team-details/team-details.component'
import { TeamService } from './team.service'
import teamData from './team.data';
import { TeamFormComponent } from './team-form/team-form.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { MusicSharedModule } from "./../music-shared/music-shared.module";

@NgModule({
  imports: [
    CommonModule,
    routerModule,
    ReactiveFormsModule,
    FormsModule,
    MusicSharedModule,
    BrowserModule
  ],
  exports: [
    TeamComponent
  ],
  declarations: [
    TeamListComponent,
    TeamDetailsComponent,
    TeamComponent,
    TeamFormComponent
  ],
  providers: [
    // TeamService,
    // { provide: 'TeamData', useValue: teamData }
  ]
})
export class TeamModule { }
