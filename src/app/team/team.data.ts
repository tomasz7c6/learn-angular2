export default [
    {
        id: 1, name: 'Lech',
        players: [
            { firstname: 'Jan' }
        ]
    },
    {
        id: 2, name: 'Wisła',
        players: [
            { firstname: 'Władysław' },
            { firstname: 'Lech' },
            { firstname: 'Miłosz' }
        ]
    },
    {
        id: 3, name: 'Pogoń',
        players: [
            { firstname: 'Arkadiusz' },
            { firstname: 'Robert' }
        ]
    }
]