import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TeamService, TeamList } from "./../team.service";


import { BrowserModule } from '@angular/platform-browser';

import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  // FormsModule,
  ReactiveFormsModule
} from "@angular/forms";

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.css'],
  styles: [`
    form {
      margin-top: 50px;
    }
  `]
})

export class TeamFormComponent implements OnInit {

  teamDetail: TeamList;
  addEditForm: FormGroup;

  constructor(
    private activeRoute: ActivatedRoute,
    private teamService: TeamService,
    private fb: FormBuilder,
    // private fg: FormGroup,
    private router: Router
  ) {


    console.log('this.searchForm', this.addEditForm)
  }
  
  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      let id = parseInt(params['id']);
      if (id) {
        this.teamService.getTeamItem(id)
        .subscribe((resp: TeamList) => {
          this.teamDetail = resp;
          this.createAddEditForm();
        },
          error => { console.log(error) });
      } else {
        this.teamDetail = this.teamService.createNewTeam();
      }
    });
  }

  private saveTeam(form) {
    console.log(form, this.addEditForm)
    if (form.invalid) {
      form.markAsDirty();
      return;
    }
    this.teamService.saveTeam(this.teamDetail)
      .subscribe(resp => {
        this.router.navigate(['team', resp.id]);
      });
  }


  private createAddEditForm() {

    this.addEditForm = this.fb.group({
      'name': new FormControl(this.teamDetail.name,
        [
          Validators.required,
          Validators.minLength(3)
        ]
      ),
      'favourites': new FormControl(false),
      'another': this.fb.group({
        'sponsor': new FormControl('Jakiś sponsor',
          [
            Validators.required
          ]),
      })
    });
  }


  public isValidField(field): Object {
    // console.log('field', field)
    let classJson = {
      'is-invalid': !field.valid && field.touched,
      'is-valid': field.valid && field.touched
    };
    // console.log('classJson', classJson)
    return classJson;
  }

}
