import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http'

@Injectable()
export class AuthService {

  constructor(private baseOptions:RequestOptions) { }

  getToken(){
    let token = localStorage.getItem('token');

    if (!token) {
      let match = window.location.hash.match(/#access_token=(.*?)&/);
      token = match && match[1];
      localStorage.setItem('token', token);
    }

    if(!token) {
      this.authorize();
    }

    this.baseOptions.headers.set('Authorization', 'Bearer ' + token)
    
    return token;
  }

  authorize() {
    localStorage.removeItem('token');
    let client_id='cfdbf840f07c4b76a7b8504e5742d6a2';
    let redirect_uri = 'http://localhost:4200/';
    let url  = 'https://accounts.spotify.com/authorize?client_id='+client_id+'&response_type=token&redirect_uri='+redirect_uri;
    window.location.replace(url);
  }

}