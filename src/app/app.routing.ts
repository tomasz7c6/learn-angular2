import { RouterModule, Routes } from '@angular/router';
// import { PlayListsComponent } from './play-lists/play-lists.component';
// import { MusicSearchComponent } from './music-search/music-search.component';

const routesConfig:Routes = [
  {path: '', redirectTo: 'music', pathMatch: 'full'},
  // {path: 'playlist', component: PlayListsComponent },
  // {path: 'music', component: MusicSearchComponent },
  {path: '**', redirectTo: 'music', pathMatch: 'full' }
]

export const routerModule = RouterModule.forRoot(routesConfig, {
  // enableTracing: true,
  // useHash: false
})