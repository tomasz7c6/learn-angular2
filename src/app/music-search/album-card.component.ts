import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.css'],
  styles: [`
    :host() {
      flex: 0 0 30% !important;
      margin-bottom: .625em !important;
      overflow: hidden;
    }
    .card-img-overlay {
      background: rgba(0,0,0,0.8);
      top:70%;
      color: #fff;
      font-size: 1em !important;
    }
  `]
})
export class AlbumCardComponent implements OnInit {

  @Input('album')
  set setAlbum(album) {
    this.album = album;
    this.image = album.images[0];
  }

  image:any;
  album:any;

  constructor() { }

  ngOnInit() {
  }

}
