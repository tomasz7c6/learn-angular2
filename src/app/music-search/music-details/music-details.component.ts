import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from './../music-search.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-music-details',
  templateUrl: './music-details.component.html',
  styleUrls: ['./music-details.component.css']
})
export class MusicDetailsComponent implements OnInit {

  constructor( private musicSearchService:MusicSearchService,
    private activatedRoute:ActivatedRoute    
  ) {}

  album;

  ngOnInit() {
    let id = this.activatedRoute
    .snapshot.params['album_id'];
    console.log('id', id, this.activatedRoute)
    this.musicSearchService.getAlbum(id)
    .subscribe(album => {
      this.album = album;
    })
  }

}
