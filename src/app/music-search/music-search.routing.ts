import { RouterModule, Routes } from '@angular/router';
// import { PlayListsComponent } from './../play-lists/play-lists.component';
import { MusicSearchComponent } from './music-search.component';
import { MusicDetailsComponent } from './music-details/music-details.component'

const routesConfig: Routes = [
    {
        path: 'music',
        component: MusicSearchComponent
    },
    {
        path: 'music/details/:album_id',
        component: MusicDetailsComponent
    }
]

export const routerModule = RouterModule.forChild(routesConfig);