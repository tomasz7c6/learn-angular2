import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { AlbumListComponent } from './album-list.component';
import { AlbumCardComponent } from './album-card.component';
import { MusicSearchService } from './music-search.service'
import { HttpModule } from '@angular/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { MusicSearchFormComponent } from './music-search-form/music-search-form.component';

import { routerModule } from './music-search.routing';
import { MusicDetailsComponent } from './music-details/music-details.component';
// import { TrackListComponent } from './track-list/track-list.component'
import { MusicSharedModule } from "./../music-shared/music-shared.module";

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    BrowserModule,
    ReactiveFormsModule,
    MusicSharedModule,
    routerModule
  ],
  declarations: [
    MusicSearchComponent,
    AlbumListComponent, 
    AlbumCardComponent, 
    MusicSearchFormComponent, 
    MusicDetailsComponent, 
    //TrackListComponent
  ],
  exports: [
    MusicSearchComponent
  ],
  providers: [
    MusicSearchService
  ]
})
export class MusicSearchModule { }
