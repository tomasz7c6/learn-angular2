import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicSearchFormComponent } from './music-search-form.component';

describe('MusicSearchFormComponent', () => {
  let component: MusicSearchFormComponent;
  let fixture: ComponentFixture<MusicSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicSearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
