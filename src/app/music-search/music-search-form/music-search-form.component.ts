import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from "./../music-search.service";
import { FormGroup, FormControl} from '@angular/forms'

@Component({
  selector: 'app-music-search-form',
  templateUrl: './music-search-form.component.html',
  styleUrls: ['./music-search-form.component.css']
})
export class MusicSearchFormComponent implements OnInit {


  searchForm:FormGroup;

  constructor(private musicSearch: MusicSearchService) { 
    this.searchForm = new FormGroup({
      'query': new FormControl('Batman')
    });

    this.searchForm.get('query').valueChanges
    .filter(query => query.length >= 3)
    .distinctUntilChanged()
    .debounceTime(400)
    .subscribe(query=>{
      this.musicSearch.search(query);
    });

  }

  search(query) {
    // this.musicSearch.search(query);
  }

  ngOnInit() {
  }

}
