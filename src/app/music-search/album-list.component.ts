import { Component, OnInit, Input } from '@angular/core';
import { MusicSearchService } from './music-search.service'

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css'],
})
export class AlbumListComponent implements OnInit {

  // albums: Array<Object>;

  albums;

  constructor(private musicSearch: MusicSearchService) {}

  ngOnInit() {
    
    // this.musicSearch.getAlbumStream()
    // .subscribe(albums => {
    //   this.albums = albums;
    // })

    this.albums = this.musicSearch.getAlbumStream();
    console.log('this.albums', this.albums)
  }

}
