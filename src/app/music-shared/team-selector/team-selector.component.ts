import { Component, OnInit } from '@angular/core';
import { TeamSelectionService } from "./../team-selection.service";
import { TeamService } from "./../../team/team.service";

@Component({
  selector: 'app-team-selector',
  templateUrl: './team-selector.component.html',
  // styleUrls: ['./team-selector.component.css']
})
export class TeamSelectorComponent implements OnInit {


  selectedId;
  teamList = [];
  constructor(private selectionService: TeamSelectionService,
    private teamService: TeamService) { }

  ngOnInit() {
    this.selectionService.getSelectedStream().subscribe(id => {
      this.selectedId = id;
    });

    this.teamService.getTeamListStream().subscribe(team => {
      this.teamList = team;
    });
  }

  setSelected(id) {
    this.selectionService.select(id)
  }

}
