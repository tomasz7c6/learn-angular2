import { Component, OnInit, Input } from '@angular/core';
import { TeamService } from "./../team/team.service";
import { TeamSelectionService } from './team-selection.service';


@Component({
  selector: 'app-track-list',
  templateUrl: './../music-search/track-list/track-list.component.html',
  // styleUrls: ['./track-list.component.css']
})
export class TrackListComponent implements OnInit {

  @Input()
  tracks;

  currentSrc = "";

  play(audio, track) {
    audio.volume = .1;
    if (audio.src != track.preview_url) {
      audio.src = track.preview_url;
      audio.play()
    } else if (audio.paused) {
      audio.src = track.preview_url;
      audio.play();
    } else {
      audio.pause();
    }
  }

  addToTeam(track) {
    this.selectionService.addToTeam(track)
  }

  // constructor(private selectionService: TeamSelectionService) { }
  constructor(private selectionService: TeamSelectionService) { }

  ngOnInit() {
  }

}
