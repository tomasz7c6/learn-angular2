import { Injectable } from '@angular/core';
import { TeamService } from "./../team/team.service";
// import { TeamService } from "../team/team.service";
import { Subject } from 'rxjs'


@Injectable()
export class TeamSelectionService {


  constructor(private teamService: TeamService) { 

    this.teamService.getTeamListStream().subscribe(team => {
      if(!this.selectedId) {
        this.select(team[0]);
      }
      
    });

  }

  selectedId;

  selectedIdStream = new Subject();

  getSelectedStream() {
    return this.selectedIdStream.startWith(this.selectedId);
  }

  select(teamId) {
    this.selectedId = teamId;
    this.selectedIdStream.next(this.selectedId);
  }

  addToTeam(track) {
    this.teamService.addToTeamList(this.selectedId, track);
  }

}
