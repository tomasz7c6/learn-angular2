import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamSelectorComponent } from './team-selector/team-selector.component';
import { TrackListComponent } from "./track-list.component";

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [
    TeamSelectorComponent,
    TrackListComponent
  ],
  exports: [
    TrackListComponent,
    TeamSelectorComponent
  ]
})
export class MusicSharedModule { }
